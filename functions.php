<?php 
/**
 * Add support for core custom logo.
 *
 * @link https://codex.wordpress.org/Theme_Logo
 */

add_theme_support( 'custom-logo', array(
	'height'      => 100,
	'width'       => 350,
	
) );