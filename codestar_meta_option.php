<?php 

require(get_template_directory().'/inc/codestar-framework/cs-framework.php'); //Must add file

// framework Theme options filter example
function industry_theme_options( $options ) {

  $options      = array(); // remove old options


  $options[]    = array(
    'name'      => 'header_option_2',
    'title'     => 'Header Option 2',
    'icon'      => 'fa fa-heart',
    'fields'    => array(
      array(
        'id'    => 'logo_text',
        'type'  => 'text',
        'title' => 'Enter Your Logo Text',
      ),
      array(
        'id'              => 'header_top_2',
        'type'            => 'group',
        'title'           => 'Header Top 2',
        'desc'            => 'Header option two ',
        'button_title'    => 'Add new',
        'accordion_title' => 'Add new link',
        'fields'          => array(
          array(
            'id'          => 'header_sub_title',
            'type'        => 'text',
            'title'       => 'Header Sub Title',
          ),

          array(
            'id'          => 'header_title',
            'type'        => 'text',
            'title'       => 'Header Title',
          ),

          array(
            'id'          => 'header_icon',
            'type'        => 'icon',
            'default' => 'fa fa-heart',
            'title'       => 'Header Icon',
          ),
          array(
            'id'          => 'icon_color',
            'type'        => 'color_picker',
            'title'       => 'Icon Color',
            'default' => '#f4cc14',
          ),
          array(
            'id'          => 'header_link',
            'type'        => 'text',
            'title'       => 'Header Link',            
          ), 
          array(
            'id'          => 'header_link_tab',
            'type'        => 'select',
            'title'       => 'Link Open In',
            'options'  => array(
              '_self'  => 'Same Tab',
              '_blank'   => 'New Tab',             
            ),            
          ),

        )
    ),
      
    )
  );